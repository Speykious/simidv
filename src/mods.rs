use crate::{Tag, Element};

pub trait TagModder {
  fn apply(&self, tag: &mut Tag) -> usize;
}

/// Specifies the XML traversing mode. Simple behaves the exact same as Level(0).
#[derive(Clone, Copy, Debug, PartialEq, Eq, Serialize, Deserialize)]
#[serde(tag = "type", rename_all = "lowercase")]
pub enum Traversing {
  Simple,
  Recursive
}

/// Different options to replace something in an XML tree.
#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case", tag = "type")]
pub enum TagModAction {
  /// Replaces the old name of a tag by the new one.
  /// If no `old_name` is provided, it will do so regardless of the tag's name.
  ReplaceName {
    old: Option<String>,
    new: String,
  },
  /// Replaces the old attribute key of a tag by the new one.
  /// Will not apply if the tag already has the new key.
  ReplaceAttrKey {
    old: String,
    new: String,
  },
  /// Replaces the old attribute value of a tag by the new one.
  /// If no `key` is provided, it will do so for every attribute of the tag.
  /// If no `old_value` is provided, it will do so regardless of the attribute's value.
  ReplaceAttrValue {
    key: Option<String>,
    old: Option<String>,
    new: String,
  },
  /// Deletes the attribute with this specific key.
  DeleteAttr {
    key: String,
  },
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct TagMod {
  pub action: TagModAction,
  pub traversing: Option<Traversing>,
}

impl TagModder for TagMod {
  /// Applies a modification to a tag, as well as all its children's if the modification's traversing is recursive.
  /// Returns the number of modifications made.
  fn apply(&self, tag: &mut Tag) -> usize {
    let mut count = 0;
    match &self.action {
      TagModAction::ReplaceName { old, new } => {
        if let Some(old) = old {
          if &tag.name == old {
            tag.name = new.to_string();
            count += 1;
          }
        } else {
          tag.name = new.to_string();
          count += 1;
        }
      },
      TagModAction::ReplaceAttrKey { old, new } => {
        if !tag.attributes.contains_key(new) {
          if let Some(value) = tag.attributes.remove(old) {
            tag.attributes.insert(new.to_string(), value);
            count += 1;
          }
        }
      },
      TagModAction::ReplaceAttrValue { key, old, new } => {
        if let Some(key) = key {
          if let Some(value) = tag.attributes.get_mut(key) {
            if let Some(old_value) = old {
              if value == old_value {
                *value = new.to_string();
                count += 1;
              }
            } else {
              *value = new.to_string();
              count += 1;
            }
          }
        } else {
          for (_, value) in tag.attributes.iter_mut() {
            if let Some(old) = &old {
              if value == old {
                *value = new.to_string();
                count += 1;
              }
            } else {
              *value = new.to_string();
              count += 1;
            }
          }
        }
      },
      TagModAction::DeleteAttr { key } => {
        if let Some(_) = tag.attributes.remove(key) {
          count += 1;
        }
      },
    }

    match self.traversing.unwrap_or(Traversing::Recursive) {
      Traversing::Simple => (),
      Traversing::Recursive => {
        for child in tag.children.iter_mut().filter_map(Element::tag_mut) {
          count += self.apply(child);
        }
      },
    }

    count

  }
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct TagMods {
  #[serde(rename = "mod")]
  pub mods: Vec<TagMod>,
}

impl TagMods {
  pub fn new() -> Self {
    TagMods { mods: Vec::new() }
  }
}

impl TagModder for TagMods {
  /// Applies all modifications to the tag at the same time.
  /// Returns the number of modifications made.
  fn apply(&self, tag: &mut Tag) -> usize {
    let mut count = 0;
    for tag_mod in &self.mods {
      count += tag_mod.apply(tag);
    }
    count
  }
}