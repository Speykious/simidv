use simidvlib::mods::{TagMod, TagModAction, Traversing, TagMods};
use std::io::{self, Write};
use std::fs::{File, read_to_string};

fn main() -> io::Result<()> {
  let tag_mods = TagMods {
    mods: vec![
      TagMod {
        action: TagModAction::ReplaceName {
          old: Some("hello".to_string()),
          new: "world".to_string(),
        },
        traversing: None,
      },
      TagMod {
        action: TagModAction::ReplaceName {
          old: None,
          new: "bye world".to_string(),
        },
        traversing: Some(Traversing::Recursive),
      },
      TagMod {
        action: TagModAction::ReplaceAttrValue {
          key: None,
          old: Some("value".to_string()),
          new: "bruh".to_string(),
        },
        traversing: Some(Traversing::Simple),
      },
    ],
  };
  
  let gen_file_name = "mods/generated.mod.toml";
  println!("Serializing example tag mods into {}", gen_file_name);
  let mut gen_mod_file = File::create(gen_file_name)?;
  write!(gen_mod_file, "{}", toml::to_string(&tag_mods).unwrap())?;

  let test_file_name = "mods/test.mod.toml";
  println!("Deserializing example tag mods from {}", test_file_name);
  let test_mod_file = read_to_string(test_file_name)?;
  println!("Got following object:\n{:#?}", toml::from_str::<TagMods>(&test_mod_file));
  
  Ok(())
}