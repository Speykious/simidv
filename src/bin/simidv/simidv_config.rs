use serde::{Serialize, Deserialize};

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct SimIDVConfig {
  pub watch_folder: Option<String>,
  pub output_folder: Option<String>,
  pub mods_file: Option<String>,
}

impl SimIDVConfig {
  pub fn from_toml(content: &str) -> Result<Self, String> {
    toml::from_str(content).map_err(|e| format!("{}", e))
  }

  pub fn to_toml(&self) -> String {
    toml::to_string(self).unwrap()
  }
}