use std::thread;

use log::*;
use nwd::NwgUi;
use nwg::{NativeUi, PartialUi};
use simidvlib::{msg, xml};
use chrono::Local;
use std::fs::{File, read_to_string};
use std::convert::TryFrom;
use std::cell::RefCell;



#[derive(Default)]
pub struct GenXMLMessageUI {
  layout: nwg::GridLayout,
  consolas: nwg::Font,
  line_inputs: Vec<nwg::TextInput>,
}

// I guess I have to build the UI without using NWG Derive at this point.
impl PartialUi for GenXMLMessageUI {
  fn build_partial<W: Into<nwg::ControlHandle>>(data: &mut Self, parent: Option<W>) -> Result<(), nwg::NwgError> {
    let parent = parent.unwrap().into();

    let mut grid_builder = nwg::GridLayout::builder()
      .parent(&parent)
      .spacing(0)
      .max_row(Some(8));
    
    nwg::Font::builder()
      .family("Consolas")
      .size(20)
      .build(&mut data.consolas)?;
    
    for i in 0..8 {
      let mut line_input = nwg::TextInput::default();

      nwg::TextInput::builder()
        .parent(&parent)
        .font(Some(&data.consolas))
        .limit(20)
        .flags(nwg::TextInputFlags::VISIBLE | nwg::TextInputFlags::TAB_STOP)
        .build(&mut line_input)?;
      
      grid_builder = grid_builder.child(0, i, &line_input);
      
      data.line_inputs.push(line_input);
    }

    grid_builder.build(&mut data.layout)?;

    Ok(())
  }
}

impl GenXMLMessageUI {
  pub fn set_enabled(&self, v: bool) {
    for line in &self.line_inputs {
      line.set_enabled(v);
    }
  }

  pub fn get_line(&self, i: usize) -> Option<msg::XMLMessageLine> {
    self.line_inputs.get(i).map(|l|
      msg::XMLMessageLine {
        num: i + 1,
        taille: None,
        gras: None,
        clignotant: None,
        texte: l.text(),
      }
    )
  }

  pub fn get_lines(&self) -> msg::XMLMessage {
    (0..self.line_inputs.len()).map(|i| self.get_line(i).unwrap()).collect()
  }

  pub fn set_line(&self, i: usize, line: &msg::XMLMessageLine) -> bool {
    self.line_inputs.get(i).map(|l| l.set_text(&line.texte)).is_some()
  }

  pub fn set_lines(&self, lines: &Vec<msg::XMLMessageLine>) -> Vec<bool> {
    lines.iter().enumerate().map(|(i, line)| self.set_line(i, line)).collect()
  }
}

#[derive(Default, NwgUi)]
pub struct GenXMLUI {
  /// Main window of the application.
  #[nwg_control(size: (500, 390), center: true, title: "SimIDV - Générer un Message XML")]
  #[nwg_events(
    OnInit: [Self::init],
    OnMinMaxInfo: [Self::set_resize(SELF, EVT_DATA)],
    OnWindowClose: [Self::exit],
  )]
  window: nwg::Window,

  /// Grid to layout objects in the "Folders" tab.
  #[nwg_layout(parent: window, spacing: 1)]
  grid: nwg::GridLayout,

  // Icons

  /// Icon for the Load button.
  #[nwg_resource(source_file: Some("resources/01_file.ico"), size: Some((32, 32)))]
  load_icon: nwg::Icon,

  /// Icon for the Save button.
  #[nwg_resource(source_file: Some("resources/06_save.ico"), size: Some((32, 32)))]
  save_icon: nwg::Icon,

  // Principal

  #[nwg_control(text: "Principal", flags: "VISIBLE|ELIPSIS", h_align: nwg::HTextAlign::Center)]
  #[nwg_layout_item(layout: grid, row: 0, col: 1, col_span: 2)]
  principal_label: nwg::Label,

  #[nwg_control(flags: "VISIBLE|BORDER")]
  #[nwg_layout_item(layout: grid, row: 1, row_span: 7, col: 0, col_span: 4)]
  principal_frame: nwg::Frame,

  #[nwg_partial(parent: principal_frame)]
  principal_ui: GenXMLMessageUI,

  #[nwg_control(text: "Alternat", flags: "VISIBLE|ELIPSIS", h_align: nwg::HTextAlign::Center)]
  #[nwg_layout_item(layout: grid, row: 0, col: 5, col_span: 2)]
  alternate_label: nwg::Label,

  #[nwg_control(flags: "VISIBLE|DISABLED|BORDER")]
  #[nwg_layout_item(layout: grid, row: 1, row_span: 7, col: 4, col_span: 4)]
  alternate_frame: nwg::Frame,

  #[nwg_partial(parent: alternate_frame)]
  alternate_ui: GenXMLMessageUI,

  #[nwg_control(text: "ID", h_align: nwg::HTextAlign::Right)]
  #[nwg_layout_item(layout: grid, row: 8, col: 0)]
  message_id_label: nwg::Label,

  #[nwg_control(text: "SimIDV.PCOM.69AV.GENERATED", placeholder_text: Some("Exemple: SimIDV.PCOM.69AV.GENERATED"))]
  #[nwg_layout_item(layout: grid, row: 8, col: 1, col_span: 3)]
  message_id_input: nwg::TextInput,

  #[nwg_control(text: "Sauvegarder", icon: Some(&data.save_icon))]
  #[nwg_layout_item(layout: grid, row: 8, col: 4, col_span: 2)]
  #[nwg_events(OnButtonClick: [Self::save_message])]
  save_btn: nwg::Button,
  
  #[nwg_control(text: "Ouvrir", icon: Some(&data.load_icon))]
  #[nwg_layout_item(layout: grid, row: 8, col: 6, col_span: 2)]
  #[nwg_events(OnButtonClick: [Self::load_message])]
  load_btn: nwg::Button,
  
  //#[nwg_control(parent: window, text: "Enable alternate message")]
  //#[nwg_layout_item(layout: grid, row: 9, col: 5, col_span: 3)]
  #[nwg_control(text: "")]
  #[nwg_layout_item(layout: grid, row: 0,  col: 4)]
  #[nwg_events(OnButtonClick: [Self::enable_alternate(SELF, CTRL)])]
  alternate_enabler: nwg::CheckBox,

  /// File dialog popup to choose a config file to load.
  #[nwg_resource(title: "Ouvrir un Message XML", action: nwg::FileDialogAction::Open, filters: "Message XML (*.xml)|Any (*.*)")]
  load_dialog: nwg::FileDialog,

  out_folder: RefCell<Option<String>>,
}

impl GenXMLUI {
  fn init(&self) {
    self.enable_alternate(&self.alternate_enabler);
  }

  pub fn set_out_folder(&self, out_folder: Option<String>) {
    *self.out_folder.borrow_mut() = out_folder;
  }

  pub fn popup(out_folder: Option<String>, sender: nwg::NoticeSender) -> thread::JoinHandle<()> {
    thread::spawn(move || {
      nwg::init().unwrap();
      let app = Self::build_ui(Self::default()).expect("Impossible de construire l'UI");
      app.set_out_folder(out_folder);
      nwg::dispatch_thread_events();
      sender.notice();
    })
  }

  fn enable_alternate(&self, cb: &nwg::CheckBox) {
    let v = cb.check_state() != nwg::CheckBoxState::Unchecked;
    self.alternate_ui.set_enabled(v);
    self.alternate_label.set_enabled(v);
    self.alternate_frame.set_enabled(v);
  }

  fn get_commande(&self) -> msg::XMLCommande {
    let now = Local::now();
    msg::XMLCommande {
      id_equipement: self.message_id_input.text(),
      horodate: format!("{}", now.format("%d/%m/%Y %T")),
      principal: self.principal_ui.get_lines(),
      alternat:
        if self.alternate_enabler.check_state() == nwg::CheckBoxState::Checked {
          Some(self.alternate_ui.get_lines())
        } else { None },
    }
  }

  fn set_commande(&self, commande: &msg::XMLCommande) {
    self.principal_ui.set_lines(&commande.principal);
    if let Some(alternat) = &commande.alternat {
      self.alternate_enabler.set_enabled(true);
      self.enable_alternate(&self.alternate_enabler);
      self.alternate_ui.set_lines(alternat);
    }
  }

  /// Loads message.
  fn load_message(&self) {
    if self.load_dialog.run(Some(&self.window)) {
      if let Ok(entry) = self.load_dialog.get_selected_item() {
        let content = match read_to_string(&entry) {
          Ok(c) => c,
          Err(err) => {
            error!("Reading error: {}", err);
            nwg::error_message("ERREUR DE LECTURE DU FICHIER", &format!("Erreur de lecture du fichier du message : {}", err));
            return;
          },
        };
        let tag = xml::Tag::from(content.as_str());
        match msg::XMLCommande::try_from(&tag) {
          Ok(commande) => self.set_commande(&commande),
          Err(err) => {
            error!("Format validation error: {}", err);
            nwg::error_message("ERREUR DE LECTURE DU MESSAGE", &format!("Erreur de lecture du contenu du message : {}", err));
          }
        }
      }
    }
  }

  /// Saves message.
  fn save_message(&self) {
    let dir = match self.out_folder.borrow().clone() {
      Some(dir) => dir,
      None => {
        nwg::error_message("PAS DE DOSSIER OUT", "Erreur : vous avez besoin de configurer un dossier OUT pour pouvoiir sauvegarder votre message.");
        return;
      }
    };
    let commande = self.get_commande();
    let tag: xml::Tag = (&commande).into();
    let mut output = File::create(
      format!("{}\\{}.1_{}.xml", &dir, commande.id_equipement, Local::now().format("%Y%m%d-%H%M%S"))
    ).unwrap();
    xml::Element::Tag(tag).write(&mut output).unwrap();
    info!("Message XML généré dans le dossier OUT ({})", &dir);
    nwg::modal_info_message(&self.window, "MESSAGE SAUVEGARDÉ", "Le message a été sauvegardé.");
  }
  
  /// This function only serves the purpose of setting the minimal size of the window.
  fn set_resize(&self, data: &nwg::EventData) {
    let data = data.on_min_max();
    data.set_min_size(500, 420);
  }

  /// Exits the application.
  fn exit(&self) {
    info!("Arrêt du popup GenXMLUI - Stoppage du thread dispatch");
    nwg::stop_thread_dispatch();
  }
}