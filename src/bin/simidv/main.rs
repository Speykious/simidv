#![windows_subsystem = "windows"]

extern crate native_windows_derive as nwd;
extern crate native_windows_gui as nwg;

use std::io;
use std::thread;
use std::sync::mpsc;
use std::path::Path;
use std::cell::RefCell;
use std::fs::{read_to_string, write};
use simidvlib::{iter_files, init_logger, WatchNext, watch_once, stop_watching, none_if_empty};
use simidvlib::mods::TagMods;

use structopt::StructOpt;
use std::path::PathBuf;
use notify::RawEvent;
use chrono::Local;
use nwg::NativeUi;
use nwd::NwgUi;
use log::*;

mod simidv_config;
mod genxml_ui;

use simidv_config::SimIDVConfig;
use genxml_ui::GenXMLUI;

#[derive(Debug, StructOpt)]
#[structopt(name = "SimIDV", about = "Simulateur de l'Information Des Voyageurs - l'imitateur de l'IDV.")]
struct Opt {
  #[structopt(short, long, parse(from_os_str))]
  pub config: Option<PathBuf>,
}

/// Structure containing the state of the entire application.
#[derive(Default, NwgUi)]
pub struct SimIDVApp {
  /// Main window of the application.
  #[nwg_control(size: (690, 420), center: true, title: "SimIDV")]
  #[nwg_events(
    OnInit: [SimIDVApp::init],
    OnMinMaxInfo: [Self::set_resize(SELF, EVT_DATA)],
    OnWindowClose: [Self::exit],
  )]
  window: nwg::Window,

  /// This grid just acts as the main frame of the window, so that I can put a tabs container inside of it and also put 1 pixel of padding.
  #[nwg_layout(parent: window, spacing: 1)]
  grid: nwg::GridLayout,

  /// Container for the tabs of the application, currently "Watch" and "Folders".
  #[nwg_control]
  #[nwg_layout_item(layout: grid)]
  tabs: nwg::TabsContainer,

  /// This is a notice. It notices the application when a separate thread has finished its job - necessary to watch a folder while also letting the graphical window run fine.
  #[nwg_control]
  #[nwg_events(OnNotice: [SimIDVApp::read_watch_event])]
  watch_event_notice: nwg::Notice,
  
  /// This variable stores the group of objects for handling the watch events, only when we're watching something.
  /// - The first object is a RawEvent message sender. It can be used to communicate to the watching thread that we want to stop, using the function `simidvlib::stop_watching(sender)`.
  /// - The second object is the object that is responsible for actually watching the folder. It is stored here so that Rust doesn't just delete the object after it has been created in the `watch_once` function. It will be automatically deleted once we stop watching.
  /// - The third object is the handle of the thread. When the watcher relays a new event, the thread sends a notice to the application that it has finished, and returns information about the event.
  watch_event_handler: RefCell<Option<(mpsc::Sender<RawEvent>, notify::RecommendedWatcher, thread::JoinHandle<WatchNext>)>>,

  #[nwg_control]
  #[nwg_events(OnNotice: [SimIDVApp::handle_genxml_exit])]
  genxml_notice: nwg::Notice,
  genxml_handler: RefCell<Option<thread::JoinHandle<()>>>,

  tagmods: RefCell<Option<TagMods>>,
  
  ///////////////////
  //// Resources ////
  ///////////////////
  
  /// Despite the existence of an image list below, I somehow still have to separately use this individually stored icon here for folders in the "Folders" tab.
  #[nwg_resource(source_file: Some("resources/04_open_folder.ico"), size: Some((32, 32)))]
  folder_icon: nwg::Icon,

  /// Despite the existence of an image list below, I somehow still have to separately use this individually stored icon here for the Save button.
  #[nwg_resource(source_file: Some("resources/05_config_file.ico"), size: Some((32, 32)))]
  config_file_icon: nwg::Icon,

  /// Despite the existence of an image list below, I somehow still have to separately use this individually stored icon here for the Save button.
  #[nwg_resource(source_file: Some("resources/06_save.ico"), size: Some((32, 32)))]
  save_icon: nwg::Icon,
  
  /// This is a list of icons that are used for the tabs' and tree view items' icons.
  #[nwg_resource(size: (16, 16))]
  image_list: nwg::ImageList,

  //////////////
  //// Menu ////
  //////////////
  
  #[nwg_control(parent: window, text: "Générer")]
  m_edit: nwg::Menu,

  #[nwg_control(parent: m_edit, text: "Message XML")]
  #[nwg_events(OnMenuItemSelected: [Self::popup_genxml])]
  mi_genxml: nwg::MenuItem,

  ///////////////////
  //// Watch tab ////
  ///////////////////
  
  /// This tab displays information on the folder being watched: a tree view of the folder on the left, events relayed on the right, and a control of the watching with the 2 buttons at the bottom.
  #[nwg_control(parent: tabs, text: "Surveillance", image_index: Some(2))]
  watch_tab: nwg::Tab,

  /// Grid to layout objects in the "Watch" tab.
  #[nwg_layout(parent: watch_tab, spacing: 1)]
  watch_grid: nwg::GridLayout,

  /// The tree view will display the folder that's being watched, as well as the files in it in real time.
  #[nwg_control]
  #[nwg_layout_item(layout: watch_grid, col: 0, col_span: 2, row: 0, row_span: 12)]
  tree_view: nwg::TreeView,

  /// Just a text that says "Latest watch events".
  #[nwg_control(text: "Derniers événements de la surveillance", h_align: nwg::HTextAlign::Center)]
  #[nwg_layout_item(layout: watch_grid, col: 2, col_span: 2, row: 0)]
  watch_events_label: nwg::Label,

  /// Displays the events when they come, along with the date and hour of occuring in a Time column.
  #[nwg_control(
    list_style: nwg::ListViewStyle::Detailed,
    ex_flags: nwg::ListViewExFlags::FULL_ROW_SELECT,
  )]
  #[nwg_layout_item(layout: watch_grid, col: 2, col_span: 2, row: 1, row_span: 9)]
  watch_events: nwg::ListView,

  /// "Watch" button to start watching.
  #[nwg_control(parent: watch_tab, text: "Surveiller", focus: true)]
  #[nwg_layout_item(layout: watch_grid, col: 2, row: 10, row_span: 2)]
  #[nwg_events(OnButtonClick: [SimIDVApp::start_watching])]
  watch_watch_btn: nwg::Button,

  /// "Stop" button to stop watching.
  #[nwg_control(parent: watch_tab, text: "Arrêter", flags: "VISIBLE|DISABLED")]
  #[nwg_layout_item(layout: watch_grid, col: 3, row: 10, row_span: 2)]
  #[nwg_events(OnButtonClick: [SimIDVApp::stop_watching])]
  watch_stop_btn: nwg::Button,

  /////////////////////
  //// Folders tab ////
  /////////////////////

  /// This tab lets you configure the folders used by the application to do its job: the folder to watch, the folder to output modified XML messages to, and the folder where mod configs are.
  #[nwg_control(parent: tabs, text: "Configuration", image_index: Some(3))]
  folders_tab: nwg::Tab,

  /// Grid to layout objects in the "Folders" tab.
  #[nwg_layout(parent: folders_tab, spacing: 1, max_row: Some(7))]
  folders_grid: nwg::GridLayout,
  
  /// Text that just says "Watch Folder" for the folder being watched.
  #[nwg_control(text: "Dossier IN", h_align: nwg::HTextAlign::Center)]
  #[nwg_layout_item(layout: folders_grid, col: 0, row: 0)]
  watch_folder_label: nwg::Label,

  /// Button to open a folder dialog to choose the folder to watch.
  #[nwg_control(text: "Ouvrir", icon: Some(&data.folder_icon))]
  #[nwg_layout_item(layout: folders_grid, col: 1, row: 0)]
  #[nwg_events(OnButtonClick: [SimIDVApp::set_watch_folder])]
  watch_folder_open_btn: nwg::Button,

  /// Readonly text that displays the path of the folder to watch when it has been set.
  /// Internally, it also serves as the storing entity for this path.
  #[nwg_control(readonly: true)]
  #[nwg_layout_item(layout: folders_grid, col: 2, col_span: 4, row: 0)]
  watch_folder_text: nwg::TextInput,
  
  /// Text that just says "Output Folder" for the folder where modified XML messages go.
  #[nwg_control(text: "Dossier OUT", h_align: nwg::HTextAlign::Center)]
  #[nwg_layout_item(layout: folders_grid, col: 0, row: 1)]
  output_folder_label: nwg::Label,

  /// Button to open a folder dialog to choose the output folder.
  #[nwg_control(text: "Ouvrir", icon: Some(&data.folder_icon))]
  #[nwg_layout_item(layout: folders_grid, col: 1, row: 1)]
  #[nwg_events(OnButtonClick: [SimIDVApp::set_output_folder])]
  output_folder_open_btn: nwg::Button,

  /// Readonly text that displays the path of the output folder when it has been set.
  /// Internally, it also serves as the storing entity for this path.
  #[nwg_control(readonly: true)]
  #[nwg_layout_item(layout: folders_grid, col: 2, col_span: 4, row: 1)]
  output_folder_text: nwg::TextInput,
  
  /// Text that just says "Mods Folder" for the folder containing all modification configs.
  #[nwg_control(text: "Fichier MODS", h_align: nwg::HTextAlign::Center)]
  #[nwg_layout_item(layout: folders_grid, col: 0, row: 2)]
  mods_file_label: nwg::Label,

  /// Button to open a folder dialog to choose the mods folder.
  #[nwg_control(text: "Ouvrir", icon: Some(&data.folder_icon))]
  #[nwg_layout_item(layout: folders_grid, col: 1, row: 2)]
  #[nwg_events(OnButtonClick: [SimIDVApp::set_mods_folder])]
  mods_file_open_btn: nwg::Button,

  /// Readonly text that displays the path of the mods folder when it has been set.
  /// Internally, it also serves as the storing entity for this path.
  #[nwg_control(readonly: true)]
  #[nwg_layout_item(layout: folders_grid, col: 2, col_span: 4, row: 2)]
  mods_file_text: nwg::TextInput,

  /// Button to open a folder dialog to choose the folder to watch.
  #[nwg_control(text: "Sauvegarder", icon: Some(&data.save_icon))]
  #[nwg_layout_item(layout: folders_grid, col: 4, row: 6)]
  #[nwg_events(OnButtonClick: [SimIDVApp::save_config])]
  save_config_btn: nwg::Button,

  /// Button to open a folder dialog to choose the folder to watch.
  #[nwg_control(text: "Ouvrir", icon: Some(&data.config_file_icon))]
  #[nwg_layout_item(layout: folders_grid, col: 5, row: 6)]
  #[nwg_events(OnButtonClick: [SimIDVApp::load_config])]
  load_config_btn: nwg::Button,

  //////////////////////
  //// File dialogs ////
  //////////////////////

  /// File dialog popup to choose the folder to watch.
  #[nwg_resource(title: "Dossier IN (à surveiller)", action: nwg::FileDialogAction::OpenDirectory)]
  watch_folder_dialog: nwg::FileDialog,

  /// File dialog popup to choose the output folder.
  #[nwg_resource(title: "Dossier OUT (à utiliser comme sortie)", action: nwg::FileDialogAction::OpenDirectory)]
  output_folder_dialog: nwg::FileDialog,
  
  /// File dialog popup to choose the mods folder.
  #[nwg_resource(title: "Fichier MODS (où se trouvent les fichiers de moditication)", action: nwg::FileDialogAction::Open, filters: "Fichier de Modification pour SimIDV (*.mod.toml)|Any (*.*)")]
  mods_file_dialog: nwg::FileDialog,

  /// File dialog popup to choose where to save the config.
  #[nwg_resource(title: "Sauvegarder la Configuration", action: nwg::FileDialogAction::Save, filters: "Fichier de Configuration pour SimIDV (*.simidv.toml)|Any (*.*)")]
  save_config_dialog: nwg::FileDialog,

  /// File dialog popup to choose a config file to load.
  #[nwg_resource(title: "Ouvrir une Configuration", action: nwg::FileDialogAction::Open, filters: "Fichier de Configuration pour SimIDV (*.simidv.toml)|Any (*.*)")]
  load_config_dialog: nwg::FileDialog,
}

impl SimIDVApp {
  /// Loads all icons from the resources folder and sets the icon of UI objects of the application.
  fn load_icons(&self) -> Result<&nwg::ImageList, nwg::NwgError> {
    let image_list = &self.image_list;
    info!("Chargement des icônes du dossier {}", "resources/");
    for icon_filename in iter_files("resources/", ".ico") {
      debug!("Chargement de l'icône {}", icon_filename);
      image_list.add_icon_from_filename(&format!("resources/{}", icon_filename))?;
    }

    self.tabs.set_image_list(Some(image_list));
    self.tree_view.set_image_list(Some(image_list));

    Ok(image_list)
  }

  fn get_config(&self) -> SimIDVConfig {
    SimIDVConfig {
      watch_folder: none_if_empty(self.watch_folder_text.text()),
      output_folder: none_if_empty(self.output_folder_text.text()),
      mods_file: none_if_empty(self.mods_file_text.text()),
    }
  }

  fn set_config(&self, config: Result<SimIDVConfig, String>) -> bool {
    info!("Chargement du fichier de configuration");
    match config {
      Ok(SimIDVConfig { watch_folder, output_folder, mods_file }) => {
        watch_folder.map(|d| {
          self.set_folder(&self.watch_folder_text, &d);
          self.reset_tree_view();
        });
        output_folder.map(|d| self.set_folder(&self.output_folder_text, &d));
        mods_file.map(|d| self.set_folder(&self.mods_file_text, &d));
        true
      },
      Err(err) => {
        let err_msg = format!("Erreur dans le fichier de configuration :\n{}", err);
        error!("{}", &err_msg);
        nwg::error_message("ERREUR DE CONFIGURATION", &err_msg);
        false
      },
    }
  }

  /// Initializes the whole application: disable the "Stop" button, load icons, and build columns for the events list view.
  fn init(&self) {
    info!("Initialisation de l'interface SimIDV...");

    // Somehow the button doesn't want to disable itself on startup
    self.watch_stop_btn.set_enabled(false);
    self.load_icons().unwrap();

    let we = &self.watch_events;
    we.insert_column(nwg::InsertListViewColumn {
      index: None,
      fmt: None,
      width: Some(120),
      text: Some("Date/Heure".to_string()),
    });
    we.insert_column(nwg::InsertListViewColumn {
      index: None,
      fmt: None,
      width: Some(180),
      text: Some("Message".to_string()),
    });
    we.set_headers_enabled(true);
  }



  /// Sets the text of a text input. That text has to be a valid directory.
  fn set_folder<P: AsRef<Path>>(&self, text_input: &nwg::TextInput, path: P) {
    let path = path.as_ref();
    if path.is_dir() {
      text_input.set_text(path.to_str().unwrap());
    } else {
      nwg::error_message("ERREUR DE CHEMIN", &format!("{:?} n'est pas un dossier !", path));
    }
  }

  /// Sets the text of a text input. That text has to be a valid file.
  fn set_file<P: AsRef<Path>>(&self, text_input: &nwg::TextInput, path: P) {
    let path = path.as_ref();
    if path.is_file() {
      text_input.set_text(path.to_str().unwrap());
    } else {
      nwg::error_message("ERREUR DE CHEMIN", &format!("{:?} n'est pas un dossier !", path));
    }
  }

  /// Boilerplate function to open a file dialog and put the information in the correct text input. Returns whether a folder has been selected.
  fn select_folder(&self, dialog: &nwg::FileDialog,  text: &nwg::TextInput) -> bool {
    if dialog.run(Some(&self.window)) {
      if let Ok(entry) = dialog.get_selected_item() {
        let dir = entry.into_string().unwrap();
        self.set_folder(text, &dir);
        return true;
      }
    }

    false
  }

  /// Boilerplate function to open a file dialog and put the information in the correct text input. Returns whether a folder has been selected.
  fn select_file(&self, dialog: &nwg::FileDialog,  text: &nwg::TextInput) -> bool {
    if dialog.run(Some(&self.window)) {
      if let Ok(entry) = dialog.get_selected_item() {
        let dir = entry.into_string().unwrap();
        self.set_file(text, &dir);
        return true;
      }
    }

    false
  }

  /// Inserts a file in the tree view.
  fn insert_file_tv(&self, root: &nwg::TreeItem, file_name: &String) {
    let tv = &self.tree_view;
    debug!("Insertion du fichier {:?} dans la vue en arbre", file_name);
    let item = tv.insert_item(file_name, Some(root), nwg::TreeInsert::Last);
    tv.set_item_image(&item, 1, false);
    tv.set_item_image(&item, 1, true);
  }

  /// Sets the watch folder by opening a file dialog.
  fn set_watch_folder(&self) {
    if self.select_folder(&self.watch_folder_dialog, &self.watch_folder_text) {
      self.reset_tree_view();
    }
  }

  /// Refreshes the tree view, in case the content of the folder to watch changed.
  fn reset_tree_view(&self) {
    let path = self.watch_folder_text.text();
    if let Some(dir) = Path::new(&path).file_name() {
      let dir = dir.to_str().unwrap();
      let tv = &self.tree_view;
      tv.clear();
      info!("Displaying watched folder {}/ in tree view", dir);
      let root = tv.insert_item(dir, None, nwg::TreeInsert::Root);
      for file_name in iter_files(path, ".xml") {
        self.insert_file_tv(&root, &file_name);
      }
      tv.set_expand_state(&root, nwg::ExpandState::Expand);
    }
  }

  /// Sets the output folder by opening a file dialog.
  fn set_output_folder(&self) {
    self.select_folder(&self.output_folder_dialog, &self.output_folder_text);
  }

  /// Sets the mods folder by opening a file dialog.
  fn set_mods_folder(&self) {
    let v = self.select_file(&self.mods_file_dialog, &self.mods_file_text);
    if v {
      let mod_file = read_to_string(self.mods_file_text.text()).unwrap();
      match toml::from_str::<TagMods>(&mod_file) {
        Ok(tagmods) => {
          self.tagmods.borrow_mut().replace(tagmods);
        },
        Err(err) => {
          let err_msg = format!("Erreur dans le fichier de modification :\n{}", err);
          error!("{}", &err_msg);
          nwg::error_message("ERREUR MOD", &err_msg);
        }
      }
    }
  }

  /// Loads config.
  fn load_config(&self) {
    if self.load_config_dialog.run(Some(&self.window)) {
      if let Ok(entry) = self.load_config_dialog.get_selected_item() {
        let content = read_to_string(entry.into_string().unwrap()).unwrap();
        let config = SimIDVConfig::from_toml(&content);
        if self.set_config(config) {
          nwg::simple_message("SUCCÈS", "La configuration a été chargée avec succès.");
        }
      }
    }
  }

  /// Saves config, including all folders.
  fn save_config(&self) {
    if self.save_config_dialog.run(Some(&self.window)) {
      if let Ok(entry) = self.save_config_dialog.get_selected_item() {
        let mut entry = entry.into_string().unwrap();
        if !entry.ends_with(".simidv.toml") {
          entry += ".simidv.toml";
        }

        let content = self.get_config().to_toml();
        match write(entry, content) {
          Ok(_) => {
            nwg::simple_message("SUCCÈS", "La configuration a été sauvegardée avec succès.");
          },
          Err(err) => {
            let err_msg = format!("Erreur lors de la sauvegarde du fichier :\n{}", err);
            error!("{}", &err_msg);
            nwg::error_message("ERREUR DE SAUVEGARDE", &err_msg);
          }
        };
      }
    }
  }


  fn popup_genxml(&self) {
    self.mi_genxml.set_enabled(false);
    *self.genxml_handler.borrow_mut() = Some(
      GenXMLUI::popup(
        none_if_empty(self.output_folder_text.text()),
        self.genxml_notice.sender()
      )
    );
  }

  fn handle_genxml_exit(&self) {
    if let Some(handle) = self.genxml_handler.borrow_mut().take() {
      handle.join().unwrap();
      self.mi_genxml.set_enabled(true);
    }
  }



  /// Spawns a new thread and starts watching the folder to watch. Will show an error dialog if there is no folder to watch set.
  fn start_watching(&self) {
    let watch_path = self.watch_folder_text.text();
    let output_path = self.output_folder_text.text();
    match watch_once(
      Some(self.watch_event_notice.sender()),
      watch_path,
      output_path,
      self.tagmods.borrow().clone(),
    ) {
      Ok((tx_stopper, watcher, handle)) => {
        let mut weh = self.watch_event_handler.borrow_mut();
        weh.replace((tx_stopper, watcher, handle));

        self.reset_tree_view();
        self.watch_watch_btn.set_enabled(false);
        self.watch_stop_btn.set_enabled(true);
      },
      Err(e) => {
        error!("Watch error: {:?}", e);
        nwg::error_message("ERREUR DE SURVEILLANCE", &format!("{}", e));
      },
    };
  }

  /// Stops watching the folder to watch.
  fn stop_watching(&self) {
    debug!("Stopped watching");
    // Disabling Stop button in any case:
    // - If there's a watcher, it's been stopped
    // - If there's no watcher, no need to stop it
    self.watch_watch_btn.set_enabled(true);
    self.watch_stop_btn.set_enabled(false);

    let mut wes = self.watch_event_handler.borrow_mut();
    if let Some((tx_stopper, _watcher, _)) = wes.take() {
      if let Err(e) = stop_watching(tx_stopper) {
        error!("Erreur de surveillance : {:?}", e);
        nwg::error_message("ERREUR DE SURVEILLANCE", &format!("{}", e));
      }
    }
  }

  /// This function is triggered once the notice receives the notification for a new event from the watching thread. It will update the content of the UI based on the event received, and also decide if it should watch again or stop.
  fn read_watch_event(&self) {
    debug!("event received!");
    let weh = self.watch_event_handler.borrow_mut().take();
    if let Some((_tx_stopper, _watcher, handle)) = weh {
      match handle.join().unwrap() {
        WatchNext::Continue(event) => {
          let event_message = match &event {
            Ok((op, file_name)) => {
              let baguette = match op {
                &notify::op::CHMOD => "CHANGEMENT DES PERMISSIONS du",
                &notify::op::CLOSE_WRITE => "ARRÊT D'ÉCRITURE dans le",
                &notify::op::CREATE => "CRÉATION du",
                &notify::op::REMOVE => "SUPPRESSION du",
                &notify::op::RENAME => "RENOMMAGE du",
                &notify::op::RESCAN => "RESCAN du",
                &notify::op::WRITE => "ÉCRITURE dans le",
                &_ => "ÉVÉNEMENT NON-IDENTIFIÉ sur le",
              };
              format!("{} fichier {}", baguette, file_name)
            },
            Err(err) => err.to_string(),
          };

          self.watch_events.insert_items_row(None, &[
            format!("{}", Local::now().format("%Y/%m/%d %T")),
            event_message,
          ]);
          
          let tv = &self.tree_view;
          let root = tv.root().unwrap();
          if let Ok((op, file_name)) = event {
            match op {
              notify::op::CREATE => self.insert_file_tv(&root, &file_name),
              notify::op::REMOVE => {
                for item in tv.iter_item(&root) {
                  if let Some(text) = tv.item_text(&item) {
                    if &text == &file_name {
                      tv.remove_item(&item);
                      break;
                    }
                  }
                }
              },
              _ => (),
            }
          }

          self.start_watching();
        },
        WatchNext::Stop => {
          self.stop_watching();
        }
      };
    }
  }

  /// This function only serves the purpose of making the minimal size of the window be 500x400.
  fn set_resize(&self, data: &nwg::EventData) {
    let data = data.on_min_max();
    data.set_min_size(500, 400);
  }

  /// Exits the application.
  fn exit(&self) {
    info!("Arrêt - Stoppage du thread dispatch");
    nwg::stop_thread_dispatch();
  }
}

/// Main. Duh.
fn main() -> io::Result<()>  {
  let opts = Opt::from_args();
  init_logger().unwrap();

  info!("Initialisation de Native Windows GUI");
  nwg::init().expect("Impossible d'initialiser Native Windows GUI");
  
  debug!("Construction de la police par défaut : Segoe UI, 18");
  let mut dfont: nwg::Font = nwg::Font::default();
  nwg::FontBuilder::new().size(18).family("Segoe UI")
  .build(&mut dfont).expect("Impossible de construire la police par défaut");

  info!("Assignation de la police par défaut : Segoe UI, 18");
  nwg::Font::set_global_default(Some(dfont));

  debug!("Construction de l'UI");
  let app = SimIDVApp::build_ui(Default::default()).expect("Impossible de construire l'UI");
  if let Opt { config: Some(config_filename) } = opts {
    let content = read_to_string(config_filename)?;
    let config = SimIDVConfig::from_toml(&content);
    app.set_config(config);
  } else {
    match read_to_string("config.simidv.toml") {
      Ok(content) => {
        let config = SimIDVConfig::from_toml(&content);
        app.set_config(config);
      },
      Err(err) => match err.kind() {
        io::ErrorKind::NotFound => (),
        _ => {
          app.set_config(Err(format!("{}", err)));
        },
      },
    };
  }

  debug!("Dispatching thread events");
  nwg::dispatch_thread_events();

  Ok(())
}
