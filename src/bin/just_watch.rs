extern crate notify;

use simidvlib::{WatchNext, watch_once};
use simidvlib::mods::TagMod;

fn main() {
  let args: Vec<String> = std::env::args().collect();
  if args.len() < 3 {
    println!("Usage: {} <watch folder> <output folder>", &args[0]);
    std::process::exit(1);
  }

  while let Ok((_, _, handle)) = watch_once::<TagMod>(None, args[1].clone(), args[2].clone(), None) {
    match handle.join().unwrap() {
      WatchNext::Continue(event) =>
        println!("Instructed to continue, received: {:?}", event),
      WatchNext::Stop =>
        println!("Instructed to stop"),
    }
  }

  println!("* dies *");
}
