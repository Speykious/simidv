use quick_xml::Reader;
use quick_xml::events::{Event, BytesStart};
use std::io::{self, BufRead, Write};
use std::string::FromUtf8Error;
use std::collections::HashMap;
use std::fmt::Display;
use std::str::FromStr;
use std::borrow::Cow;
use std::fmt;

const TAB: &str = "  ";

pub type Attributes = HashMap<String, String>;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Element { Tag(Tag), Text(String) }

impl Element {
  pub fn is_tag(&self) -> bool {
    match self {
      Element::Tag(_) => true,
      _ => false,
    }
  }

  pub fn is_text(&self) -> bool {
    match self {
      Element::Text(_) => true,
      _ => false,
    }
  }

  pub fn tag(&self) -> Option<&Tag> {
    match self {
      Element::Tag(t) => Some(t),
      _ => None,
    }
  }

  pub fn tag_mut(&mut self) -> Option<&mut Tag> {
    match self {
      Element::Tag(t) => Some(t),
      _ => None,
    }
  }

  pub fn text(&self) -> Option<&String> {
    match self {
      Element::Text(t) => Some(t),
      _ => None,
    }
  }

  pub fn write_indented<W: Write>(&self, w: &mut W, level: usize) -> io::Result<()> {
    let tabs = TAB.repeat(level);
  
    match self {
      Element::Tag(tag) => {
        write!(
          w, "\n{}<{}{}", tabs, &tag.name,
          tag.attributes.iter().fold(
            String::new(),
            |s, (k2, v2)| s + &format!(" {}={:?}", k2, v2),
          ),
        )?;
        
        // Didn't know you could get a slice from a Vec with this ref-deref trick! Amazing
        match &*tag.children {
          [] =>
            write!(w, " />"),
          [Element::Text(only_child)] =>
            write!(w, ">{}</{}>", only_child, &tag.name),
          _ => {
            write!(w, ">")?;
            for child in &tag.children {
              child.write_indented(w, level + 1)?;
            }
            write!(w, "\n{}</{}>", tabs, &tag.name)
          }
        }
      },
      Element::Text(text) => {
        write!(w, "\n{}{}", tabs, text)
      }
    }
  }
  
  pub fn write<W: Write>(&self, w: &mut W) -> io::Result<()> {
    write!(w, "<?xml version=\"1.0\" standalone=\"yes\"?>")?;
    self.write_indented(w, 0)
  }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Tag {
  pub name: String,
  pub local_name: String,
  pub attributes: Attributes,
  pub children: Vec<Element>,
}

impl Tag {
  pub fn new<S: ToString>(name: S) -> Self {
    let name = name.to_string();
    Tag {
      name: name.clone(),
      local_name: name,
      attributes: HashMap::new(),
      children: Vec::new(),
    }
  }

  pub fn with_attribute<K: ToString, V: ToString>(mut self, key: K, value: V) -> Self {
    self.attributes.insert(key.to_string(), value.to_string());
    self
  }

  pub fn with_attribute_opt<K: ToString, V: ToString>(mut self, key: K, value: Option<V>) -> Self {
    if let Some(value) = value {
      self.attributes.insert(key.to_string(), value.to_string());
    }
    self
  }

  pub fn with_attributes<K, V, I>(mut self, attributes: I) -> Self
  where K: ToString,
        V: ToString,
        I: IntoIterator<Item = (K, V)>, {
    self.attributes.extend(
      attributes.into_iter().map(|(k, v)| (k.to_string(), v.to_string()))
    );
    self
  }

  pub fn with_child(mut self, child: Element) -> Self {
    self.children.push(child);
    self
  }

  pub fn with_tag(self, tag: Tag) -> Self {
    self.with_child(Element::Tag(tag))
  }

  pub fn with_tag_opt(mut self, tag: Option<Tag>) -> Self {
    if let Some(tag) = tag {
      self.children.push(Element::Tag(tag));
    }
    self
  }

  pub fn with_text<S: ToString>(self, text: S) -> Self {
    self.with_child(Element::Text(text.to_string()))
  }

  pub fn with_children<I>(mut self, children: I) -> Self
  where I: IntoIterator<Item = Element>, {
    self.children.extend(children);
    self
  }

  pub fn with_tags<I>(self, tags: I) -> Self
  where I: IntoIterator<Item = Tag>, {
    self.with_children(tags.into_iter().map(Element::Tag))
  }
}

impl From<&BytesStart<'_>> for Tag {
  fn from(e: &BytesStart) -> Tag {
    let name = String::from_utf8(e.name().to_vec()).unwrap();
    let local_name = String::from_utf8(e.local_name().to_vec()).unwrap();
    let attributes = e.attributes()
      .map(|a| {
        let a = a.unwrap();
        let key = String::from_utf8(a.key.to_vec()).unwrap();
        let value = cow_slice_to_string(a.value).unwrap();
        (key, value)
      })
      .collect::<HashMap<_, _>>();
    
    Tag {
      name,
      local_name,
      attributes,
      children: Vec::new(),
    }
  }
}

impl<B: BufRead> From<&mut Reader<B>> for Tag {
  fn from(reader: &mut Reader<B>) -> Tag {
    let mut buf = Vec::new();
    let mut level: usize = 0;
    let mut tag_stack = Vec::new();
    
    loop {
      match reader.read_event(&mut buf) {
        Ok(Event::Start(ref e)) => {
          let tag = Tag::from(e);
          tag_stack.push(tag);
          level += 1;
        },
        Ok(Event::End(ref _e)) => {
          // I thought about checking whether the end matched the start tag
          // but it seems that quick_xml already deals with it on its own.
          if level <= 1 { break; }
          let tag = tag_stack.pop().unwrap();
          level -= 1;
          tag_stack[level - 1].children.push(Element::Tag(tag));
        },
        // unescape and decode the text event using the reader encoding
        Ok(Event::Text(e)) => {
          let text = Element::Text(e.unescape_and_decode(&reader).unwrap());
          tag_stack[level - 1].children.push(text);
        },
        Ok(Event::Eof) => break, // exits the loop when reaching end of file
        Err(e) => panic!("\x1b[1m\x1b[31mErreur à la position\x1b[0m {}: {:?}", reader.buffer_position(), e),
        _ => (), // There are several other `Event`s we do not consider here
      }
  
      // if we don't keep a borrow elsewhere, we can clear the buffer to keep memory usage low
      buf.clear();
    }

    tag_stack.pop().unwrap()
  }
}

impl From<&str> for Tag {
  fn from(xml: &str) -> Tag {
    let mut reader = Reader::from_str(xml);
    reader.trim_text(true);
    Tag::from(&mut reader)
  }
}

fn cow_slice_to_string<'a>(slice: Cow<'a, [u8]>) -> Result<String, FromUtf8Error> {
  String::from_utf8(match slice {
    Cow::Borrowed(slice) => slice.to_vec(),
    Cow::Owned(slice) => slice,
  })
}

pub fn debug_xml(xml: &str) {
  let mut reader = Reader::from_str(xml);
  reader.trim_text(true);
  
  let mut buf = Vec::new();
  let mut indentation = 0;
  let mut tags = Vec::new();
  
  loop {
    match reader.read_event(&mut buf) {
      Ok(Event::Start(ref e)) => {
        let name = String::from_utf8(e.name().to_vec()).unwrap();
        let local_name = String::from_utf8(e.local_name().to_vec()).unwrap();
        let attributes = e.attributes()
          .map(|a| {
            let a  = a.unwrap();
            (String::from_utf8(a.key.to_vec()).unwrap(), cow_slice_to_string(a.value).unwrap())
          })
          .collect::<HashMap<_, _>>();
        
        tags.push(name.clone());
        
        println!("{}\x1b[1mDébut\x1b[0m du tag \x1b[32m{}\x1b[0m ({}) {:?}", TAB.repeat(indentation), &name, local_name, attributes);
        indentation += 1;
      },
      Ok(Event::End(ref e)) => {
        indentation -= 1;
        let name = String::from_utf8(e.name().to_vec()).unwrap();
        let local_name = String::from_utf8(e.local_name().to_vec()).unwrap();
        tags.pop();

        println!("{}\x1b[1mFin\x1b[0m du tag \x1b[33m{}\x1b[0m ({})", TAB.repeat(indentation), &name, local_name);
      }
      // unescape and decode the text event using the reader encoding
      Ok(Event::Text(e)) => println!("{}\x1b[1mTexte\x1b[0m: {}", TAB.repeat(indentation), e.unescape_and_decode(&reader).unwrap()),
      Ok(Event::Eof) => break, // exits the loop when reaching end of file
      Err(e) => panic!("\x1b[1m\x1b[31mErreur à la position\x1b[0m {}: {:?}", reader.buffer_position(), e),
      _ => (), // There are several other `Event`s we do not consider here
    }

    // if we don't keep a borrow elsewhere, we can clear the buffer to keep memory usage low
    buf.clear();
  }
}



/* Validation */

#[derive(Debug, PartialEq, Eq)]
pub enum FormatValidationError {
  WrongElementType {
    expected: String,
    actual: String,
  },
  WrongTagName {
    expected: String,
    actual: String,
  },
  InvalidAttribute {
    tag: String,
    attribute: String,
  },
  InvalidAttributeValue {
    tag: String,
    attribute: String,
    value: String,
    error: String,
  },
  MissingAttribute {
    tag: String,
    attribute: String,
  },
  InvalidChild {
    tag: String,
    child: String,
    error: String,
  },
  MissingChild {
    tag: String,
    child: String,
  },
  MissingText {
    tag: String,
  }
}

impl fmt::Display for FormatValidationError {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self {
      FormatValidationError::WrongElementType { expected, actual } =>
        write!(f, "Mauvais type d'élément : {:?} attendu, {:?} reçu", expected, actual),
      FormatValidationError::WrongTagName { expected, actual } =>
        write!(f, "Mauvais nom de tag: {:?} attendu, {:?} reçu", expected, actual),
      FormatValidationError::InvalidAttribute { tag, attribute } =>
        write!(f, "Attribut invalide : {:?} du tag {:?}", attribute, tag),
      FormatValidationError::InvalidAttributeValue { tag, attribute, value, error } =>
        write!(f, "Valeur d'attribut invalide : {:?} de l'attribut {:?} du tag {:?} | {}", value, attribute, tag, error),
      FormatValidationError::MissingAttribute { tag, attribute } =>
        write!(f, "Attribut manquant : {:?} du tag {:?}", attribute, tag),
      FormatValidationError::InvalidChild { tag, child, error } =>
        write!(f, "Enfant invalide : {:?} du tag {:?} | {}", child, tag, error),
      FormatValidationError::MissingChild { tag, child } =>
        write!(f, "Enfant manquant : {:?} of tag {:?}", child, tag),
      FormatValidationError::MissingText { tag } =>
        write!(f, "Texte manquant à l'intérieur d'un tag : {:?}", tag),
    }
  }
}

pub type FormatValidationErrors = Vec<FormatValidationError>;
pub type FormatValidationResult<T> = Result<T, FormatValidationError>;

impl Tag {
  pub fn validate_tag_name(&self, expected: &str)
  -> FormatValidationResult<&Self> {
    if &self.name == &expected {
      Ok(self)
    } else {
      Err(
        FormatValidationError::WrongTagName {
          expected: expected.to_string(),
          actual: self.name.clone(),
        }
      )
    }
  }

  pub fn validate_attribute(&self, attribute: &str)
  -> FormatValidationResult<String> {
    match self.attributes.get(attribute) {
      Some(value) => Ok(value.clone()),
      None => Err(
        FormatValidationError::MissingAttribute {
          tag: self.name.clone(),
          attribute: attribute.to_string(),
        }
      ),
    }
  }

  pub fn parse_attribute_value<T>(&self, attribute: &str, value: &str)
  -> FormatValidationResult<T>
  where T: FromStr,
        T::Err: Display, {
    match value.parse::<T>() {
      Ok(t) => Ok(t),
      Err(err) => return Err(
        FormatValidationError::InvalidAttributeValue {
          tag: self.name.to_string(),
          attribute: attribute.to_string(),
          value: value.to_string(),
          error: format!("{}", err),
        }
      ),
    }
  }

  pub fn parse_attribute<T>(&self, attribute: &str)
  -> FormatValidationResult<T>
  where T: FromStr,
        T::Err: Display, {
    self.parse_attribute_value(attribute, &self.validate_attribute(attribute)?)
  }
  
  pub fn parse_attribute_opt<T>(&self, attribute: &str)
  -> FormatValidationResult<Option<T>>
  where T: FromStr,
        T::Err: Display, {
    let value = match self.validate_attribute(attribute) {
      Ok(value) => value,
      Err(_) => return Ok(None),
    };

    Ok(Some(self.parse_attribute_value(attribute, &value)?))
  }

  pub fn validate_attributes<'b, I>(&self, attributes: &mut I)
  -> FormatValidationResult<&Self>
  where I: Iterator<Item = &'b str> {
    for attribute in attributes {
      self.validate_attribute(attribute)?;
    }

    Ok(self)
  }

  /// verifies that the XML tag contains at least one child named after the expected string.
  pub fn validate_child(&self, expected: &str)
  -> FormatValidationResult<&Self> {
    let expected = expected.to_string();

    for child in self.children.iter().filter_map(Element::tag) {
      if child.name == expected {
        return Ok(child);
      }
    }

    Err(FormatValidationError::MissingChild {
      tag: self.name.clone(),
      child: expected,
    })
  }

  pub fn validate_text_child(&self) -> FormatValidationResult<String> {
    let mut children = self.children.iter();
    let text = match children.next() {
      Some(Element::Text(s)) => s.clone(),
      Some(_) => return Err(
        FormatValidationError::WrongElementType {
          expected: "Text".to_string(),
          actual: "Tag".to_string(),
        }
      ),
      None => return Err(
        FormatValidationError::MissingText {
          tag: self.name.clone(),
        }
      ),
    };

    match children.next() {
      None => Ok(text),
      Some(child) => Err(
        FormatValidationError::InvalidChild {
          tag: self.name.clone(),
          child: match child {
            Element::Text(_) => "(Text element)".to_string(),
            Element::Tag(t) => t.name.clone(),
          },
          error: "Enfant additionnel non-autorisé".to_string(),
        }
      ),
    }
  }

  pub fn validate_message(&self)
  -> FormatValidationResult<&Self> {
    for child in self.children.iter().filter_map(Element::tag) {
      match child.name.as_str() {
        "LIGNE" => {
          child.validate_attributes(&mut vec!["num", "taille"].into_iter())?;
          let bloc = child.validate_child("BLOC")?;
          bloc.validate_attributes(&mut vec!["gras", "clignotant"].into_iter())?;
        },
        "PARAMETRES" => (),
        _ => {
          return Err(
            FormatValidationError::WrongTagName {
              expected: "LIGNE|PARAMETRES".to_string(),
              actual: child.name.clone(),
            }
          );
        }
      }
    }

    Ok(self)
  }

  // I really wonder if I can make this function more efficient while still being clean and not overengineered.
  pub fn validate_commande(&self)
  -> FormatValidationResult<&Self> {
    self.validate_tag_name("COMMANDE")?;

    let entete = self.validate_child("ENTETE")?;
    let corps = self.validate_child("CORPS")?;

    entete.validate_child("ID_EQUIPEMENT")?;
    entete.validate_child("TYPE_COMMANDE")?;
    entete.validate_child("HORODATE")?;
    
    let configuration = corps.validate_child("CONFIGURATION")?;
    let message = configuration.validate_child("MESSAGE")?;
    let principal = message.validate_child("PRINCIPAL")?;
    principal.validate_message()?;

    if let Ok(alternat) = message.validate_child("ALTERNAT") {
      alternat.validate_message()?;
    }

    Ok(self)
  }
}
