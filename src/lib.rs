/*
Goals:
- having a system of generic and configurable XML file transformations.
  Those modifications can include:
  - replacing a tag, attribute (or value?) name
  - deleting a tag and all its content
  - removing a tag and unnesting its content
  - adding a tag somewhere
  The concept of a modification thus perfectly maps to a trait that has to implement a transformation function.
*/

extern crate log;
extern crate simplelog;
#[macro_use] extern crate serde;
extern crate native_windows_gui as nwg;

pub mod xml;
pub mod msg;
pub mod mods;

use mods::TagModder;

use log::*;
use simplelog::*;
use chrono::Local;
use xml::{Element, Tag};
use notify::{raw_watcher, RawEvent, RecursiveMode, Watcher};
use std::sync::mpsc::{channel, Receiver, Sender, SendError};
use std::fs::{read_dir, read_to_string};
use std::time::Duration;
use std::fmt::Display;
use std::path::Path;
use std::fs::File;
use std::thread;
use std::env;

pub fn none_if_empty(s: String) -> Option<String> {
  if s.is_empty() {
    None
  } else {
    Some(s)
  }
}

/// Shortcut to iterate over all the files present in a given folder.
pub fn iter_files<P: AsRef<Path>>(path: P, ext: &'static str) -> impl Iterator<Item = String> {
  read_dir(path).unwrap()
  .filter_map(|e| e.ok())
  .filter_map(|e| e.file_name().into_string().ok())
  .filter(move |e| e.ends_with(ext))
}

/// Initializes the logger of the application. It will initialize the log level based on the LOG_LEVEL environment variable of the executed program.
pub fn init_logger() -> Result<(), SetLoggerError> {
  let log_filter = match env::var("LOG_LEVEL").unwrap_or("WARN".to_string()).as_str() {
    "E" | "ERR"  | "ERROR"   => LevelFilter::Error,
    "W" | "WARN" | "WARNING" => LevelFilter::Warn,
    "D" | "DBG"  | "DEBUG"   => LevelFilter::Debug,
    "T" | "TRC"  | "TRACE"   => LevelFilter::Trace,
    "O" | "OFF"              => LevelFilter::Off,
    "S" | "SILENT"           => return Ok(()),
    _                        => LevelFilter::Info,
  };

  let log_filename = format!("{}", Local::now().format("logs/SIMIDV.%F_%H-%M-%S%.3f.log"));
  std::fs::create_dir_all("logs").unwrap();
  CombinedLogger::init(vec![
    WriteLogger::new(
      log_filter,
      Config::default(),
      File::create(log_filename).unwrap(),
    ),
  ])
}

/// Processes an XML message. If it manages to read the file, it will verify that it is valid, then apply modifications to it and send that to the output folder.
pub fn process_xml_message<P, S1, S2, T>(path: P, file_name: S1, output_dir: S2, tag_modder: &T) -> Result<(), String>
where P: AsRef<Path>,
      S1: ToString + Display,
      S2: ToString + Display,
      T: TagModder, {
  let content = match read_to_string(&path) {
    Ok(c) => c,
    Err(err) => {
      error!("Reading error: {}", err);
      return Err(format!("{}", err));
    },
  };
  let mut tag = Tag::from(content.as_str());
  
  match tag.validate_commande() {
    Ok(_) => info!("Le message XML est valide"),
    Err(err) => {
      error!("Erreur de validation du format : {}", err);
      return Err(format!("{}", err));
    },
  };

  tag_modder.apply(&mut tag);
  info!("Message XML modifié");
  
  let mut output = File::create(format!("{}/{}", output_dir, file_name)).unwrap();
  Element::Tag(tag).write(&mut output).unwrap();
  info!("Message XML envoyé dans le dossier OUT ({})", output_dir);

  Ok(())
}

/// Whether to continue watching, or to stop.
#[derive(PartialEq, Eq, Clone, Debug)]
pub enum WatchNext { Continue(Result<(notify::Op, String), String>), Stop }

/// Waits for the next event to come, and reacts to it. It will either return an error if there's a problem, process the XML message, or return the action of stopping.
fn react_to_event<T: TagModder>(rx_watch: Receiver<RawEvent>, output_dir: String, tag_modder: &Option<T>) -> WatchNext {
  debug!("En attente du prochain événement...");
  let result = match rx_watch.recv() {
    Ok(RawEvent { path: Some(path), op: Ok(op), cookie: _ }) => {
      let path_text = path.to_str().unwrap();
      let file_name = path.as_path().file_name().unwrap().to_str().unwrap();
      match op {
        notify::op::CREATE => {
          info!("Nouveau fichier {} détecté", &path_text);
          thread::sleep(Duration::from_millis(100)); // Nobody will notice anything <u<
          
          if let Some(tag_modder) = tag_modder {
            if let Err(s) = process_xml_message(&path, file_name, &output_dir, tag_modder) {
              return WatchNext::Continue(Err(s));
            }
          }
        },
        _ => (),
      }
      WatchNext::Continue(Ok((op, file_name.to_string())))
    },
    // Complete hack I found to be able to stop the thread on-demand
    Ok(RawEvent { path: None, op: Ok(notify::op::CLOSE_WRITE), cookie: None }) => {
      info!("Événement secret détecté ! Destruction automatique...");
      WatchNext::Stop
    },
    Ok(e) => {
      let slog = format!("Événement non-valide : {:?}", e);
      error!("{}", &slog);
      WatchNext::Continue(Err(slog))
    },
    Err(e) => {
      let slog = format!("Erreur RECV: {:?}", e);
      error!("{}", &slog);
      WatchNext::Continue(Err(slog))
    },
  };

  result
}

/// Spawns a thread to watch a folder, then stops as soon as an event is received while sending a notice to whatever wants to in the application, if a notice sender is specified.
pub fn watch_once<T: TagModder + Send + 'static>(
  sender: Option<nwg::NoticeSender>,
  watch_dir: String,
  output_dir: String,
  tag_modder: Option<T>,
) -> notify::Result<(Sender<RawEvent>, notify::ReadDirectoryChangesWatcher, thread::JoinHandle<WatchNext>)> {
  if watch_dir.is_empty() {
    return Err(notify::Error::Generic("Dossier à Surveiller non specifié!".to_string()));
  }
  if !Path::new(&watch_dir).is_dir() {
    return Err(notify::Error::Generic("Dossier à Surveiller non trouvé!".to_string()));
  }
  if output_dir.is_empty() {
    return Err(notify::Error::Generic("Dossier OUT non specifié!".to_string()));
  }
  if !Path::new(&output_dir).is_dir() {
    return Err(notify::Error::Generic("Dossier OUT non trouvé!".to_string()));
  }
  // Turned into a raw watcher
  let (tx_watch, rx_watch) = channel();
  let tx_stopper = tx_watch.clone();
  let mut watcher = raw_watcher(tx_watch)?;

  // Add a path to be watched. All files and directories
  // at that path and below will be monitored for changes.
  match watcher.watch(&watch_dir, RecursiveMode::NonRecursive) {
    Ok(_) => info!("En surveillance du dossier {}", &watch_dir),
    Err(err) => return Err(err),
  };

  let handle = thread::spawn(move || {
    let watch_reaction = react_to_event(rx_watch, output_dir, &tag_modder);
    if let Some(sender) = sender { sender.notice(); }
    watch_reaction
  });

  Ok((tx_stopper, watcher, handle))
}

/// Uses a sender to send a fake event to the watching thread to inform it to stop watching.
pub fn stop_watching(tx_stopper: Sender<RawEvent>) -> Result<(), SendError<RawEvent>> {
  tx_stopper.send(RawEvent {
    path: None,
    op: Ok(notify::op::CLOSE_WRITE),
    cookie: None,
  })
}