use crate::xml;
use std::convert::TryFrom;

#[derive(Debug, Clone)]
pub struct XMLMessageLine {
  pub num: usize,
  pub taille: Option<usize>,
  pub gras: Option<bool>,
  pub clignotant: Option<bool>,
  pub texte: String,
}

impl TryFrom<&xml::Tag> for XMLMessageLine {
  type Error = xml::FormatValidationError;

  fn try_from(tag: &xml::Tag) -> xml::FormatValidationResult<Self> {
    tag.validate_tag_name("LIGNE")?;
    let num = tag.parse_attribute::<usize>("num")?;
    let taille = tag.parse_attribute_opt::<usize>("taille")?;
    let bloc = tag.validate_child("BLOC")?;
    let gras = bloc.parse_attribute_opt::<bool>("gras")?;
    let clignotant = bloc.parse_attribute_opt::<bool>("clignotant")?;
    let texte = bloc.validate_text_child().unwrap_or(String::new());
    Ok(XMLMessageLine { num, taille, gras, clignotant, texte })
  }
}

impl Into<xml::Tag> for &XMLMessageLine {
  fn into(self) -> xml::Tag {
    xml::Tag::new("LIGNE")
    .with_attribute("num", self.num)
    .with_attribute_opt("taille", self.taille)
    .with_tag(
      xml::Tag::new("BLOC")
      .with_attribute_opt("gras", self.gras)
      .with_attribute_opt("clignotant", self.clignotant)
      .with_text(&self.texte)
    )
  }
}

pub type XMLMessage = Vec<XMLMessageLine>;

impl TryFrom<&xml::Tag> for XMLMessage {
  type Error = xml::FormatValidationError;

  fn try_from(tag: &xml::Tag) -> xml::FormatValidationResult<Self> {
    let mut message = Vec::new();
    for child in tag.children.iter().filter_map(xml::Element::tag) {
      message.push(
        match XMLMessageLine::try_from(child) {
          Ok(line) => line,
          Err(err) => if child.name == "PARAMETRES" { continue } else { return Err(err) },
        }
      );
    }

    Ok(message)
  }
}

pub struct XMLCommande {
  pub id_equipement: String,
  pub horodate: String,
  pub principal: XMLMessage,
  pub alternat: Option<XMLMessage>,
}

impl TryFrom<&xml::Tag> for XMLCommande {
  type Error = xml::FormatValidationError;

  fn try_from(tag: &xml::Tag) -> xml::FormatValidationResult<Self> {
    tag.validate_tag_name("COMMANDE")?;

    let entete = tag.validate_child("ENTETE")?;
    let corps = tag.validate_child("CORPS")?;

    let id_equipement = entete.validate_child("ID_EQUIPEMENT")?.validate_text_child()?;
    let type_commande = entete.validate_child("TYPE_COMMANDE")?.validate_text_child()?;
    if &type_commande != "MSG" {
      return Err(
        xml::FormatValidationError::InvalidChild {
          tag: "TYPE_COMMANDE".to_string(),
          child: "(élément Text)".to_string(),
          error: "Le tag devait contenir le texte \"MSG\"".to_string(),
        }
      );
    }
    let horodate = entete.validate_child("HORODATE")?.validate_text_child()?;
    
    let configuration = corps.validate_child("CONFIGURATION")?;
    let message = configuration.validate_child("MESSAGE")?;

    let principal = XMLMessage::try_from(message.validate_child("PRINCIPAL")?)?;
    let alternat = match message.validate_child("ALTERNAT") {
      Ok(child) => Some(XMLMessage::try_from(child)?),
      Err(_) => None,
    };

    Ok(XMLCommande { id_equipement, horodate, principal, alternat })
  }
}

impl Into<xml::Tag> for &XMLCommande {
  fn into(self) -> xml::Tag {
    xml::Tag::new("COMMANDE").with_tags(vec![
      xml::Tag::new("ENTETE").with_tags(vec![
        xml::Tag::new("ID_EQUIPEMENT").with_text(&self.id_equipement),
        xml::Tag::new("TYPE_COMMANDE").with_text("MSG"),
        xml::Tag::new("HORODATE").with_text(&self.horodate),
      ]),
      xml::Tag::new("CORPS").with_tag(
        xml::Tag::new("CONFIGURATION").with_tag(
          xml::Tag::new("MESSAGE").with_tag(
            xml::Tag::new("PRINCIPAL").with_tags(
              self.principal.iter().map(|m| m.into()).collect::<Vec<_>>()
            ).with_tag(xml::Tag::new("PARAMETRES"))
          ).with_tag_opt(
            self.alternat.as_ref().map(|alternat|
              xml::Tag::new("ALTERNAT").with_tags(
                alternat.iter().map(|m| m.into()).collect::<Vec<_>>()
              ).with_tag(xml::Tag::new("PARAMETRES"))
            )
          )
        )
      ),
    ])
  }
}