; simidv.nsi

;--------------------------------

Name "SimIDV"

OutFile "Install_SimIDV_64bit.exe"

RequestExecutionLevel admin

Unicode True

InstallDir $PROGRAMFILES64\SimIDV
InstallDirRegKey HKLM "Software\SimIDV" "Install_Dir"

;--------------------------------

Page license
Page components
Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

LoadLanguageFile "${NSISDIR}\Contrib\Language files\French.nlf"
LoadLanguageFile "${NSISDIR}\Contrib\Language files\English.nlf"

LicenseLangString licenseData ${LANG_ENGLISH} "LICENSE"
LicenseLangString licenseData ${LANG_FRENCH} "LICENSE_fr-FR"

LicenseData $(licenseData)

;--------------------------------

; The stuff to install
Section "SimIDV (required)"

  SectionIn RO
  
  CreateDirectory $INSTDIR\resources
  SetOutPath $INSTDIR\resources
  File /r resources\*.*

  SetOutPath $INSTDIR
  File /oname=SimIDV.exe target\release\simidv.exe
  
  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\SimIDV "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SimIDV" "DisplayName" "Uninstall SimIDV"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SimIDV" "UninstallString" '"$INSTDIR\Uninstall_SimIDV.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SimIDV" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SimIDV" "NoRepair" 1
  WriteUninstaller "$INSTDIR\Uninstall_SimIDV.exe"
  
SectionEnd

; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

  CreateDirectory "$SMPROGRAMS\SimIDV"
  CreateShortcut "$SMPROGRAMS\SimIDV\Uninstall.lnk" "$INSTDIR\Uninstall_SimIDV.exe"
  CreateShortcut "$SMPROGRAMS\SimIDV\SimIDV (32 bit).lnk" "$INSTDIR\SimIDV.exe"

SectionEnd

;--------------------------------

; Uninstaller

Section "Uninstall"
  
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SimIDV"
  DeleteRegKey HKLM SOFTWARE\SimIDV

  ; Remove files and uninstaller
  Delete $INSTDIR\resources\*
  RMDir $INSTDIR\resources
  Delete $INSTDIR\logs\*
  RMDir $INSTDIR\logs
  Delete $INSTDIR\*

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\SimIDV\*.lnk"

  ; Remove directories
  RMDir "$SMPROGRAMS\SimIDV"
  RMDir "$INSTDIR"

SectionEnd

;--------------------------------

Function .onInit

	;Language selection dialog

	Push ""
	Push ${LANG_ENGLISH}
	Push English
	Push ${LANG_FRENCH}
	Push French
	Push A ; A means auto count languages
	       ; for the auto count to work the first empty push (Push "") must remain
	LangDLL::LangDialog "Installer Language" "Please select the language."

	Pop $LANGUAGE
	StrCmp $LANGUAGE "cancel" 0 +2
		Abort
FunctionEnd